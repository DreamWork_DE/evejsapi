# EvE Online Javascript API
**Ein asynchroner EVE Online API Client für Node.js**

- asynchron
- Promise basierend
- umfangreiches Caching

### Installation
```
npm install evejsapi --save
```

### Beispiele
#### XML Client

```javascript
const evejsapi = require('evejsapi');
console.log(evejsapi);
```
```json
{
  cache: {
    file: [Function: FileCache],
    memory: [Function: MemoryCache],
    mongo: [Function: MongoDBCache],
    redis: [Function: RedisCache] },
  client: {
    evecentral: [Function: EveCentralClient],
    xml: [Function: XmlClient]
  }
}
```

**Memory Cache**

Wenn der Instance des Clients kein Cache Object übergeben wird, so wird default der MemoryCache genutzt.

```javascript
const evejsapi = require('evejsapi');

const XmlClient = new evejsapi.client.xml();

XmlClient.fetch('server:ServerStatus')
  .then((data) => {
    console.log('data');

  })
  .catch(console.error);
```
**File Cache**

Folgende Keys sind möglich

- path  (absoluter Pfad) | default: /tmp/evejsapi-cache
- prefix | default: ''

```javascript
...
const XmlClient = new evejsapi.client.xml({
  cache: new evejsapi.cache.file({
    path: '/tmp/evejsapi-cache',
    prefix: ''
  }),
});
...
```

**MongoDB Cache**

Folgende Keys sind möglich

- host | default: localhost
- port | default: 27017
- collection | default: evejsapi
- prefix | default: ''

```javascript
...
const XmlClient = new evejsapi.client.xml({
  cache: new evejsapi.cache.mongo({
    host: 'localhost',
    port: 27017,
    collection: 'evejsapi',
    prefix: ''
  }),
});
...
```

**Redis Cache**

Folgende Keys sind möglich

- socket

**oder**

- host | default: localhost
- port | default: 6379
- prefix | default: 'evejsapi.cache'

```javascript
...
const XmlClient = new evejsapi.client.xml({
  cache: new evejsapi.cache.redis({
    host: 'localhost',
    port: 6379,
    prefix: 'evejsapi.cache'
  }),
});
...
```

#### EvE Central Client
Der Client kann folgende Calls ausführen

EvE-Central API https://eve-central.com/home/develop.html

Alle Abfragen werden für 5 Minuten gecached.

- marketstat
- quicklook
- quicklook/onpath

```javascript
const evejsapi = require('evejsapi');

const XmlClient = new evejsapi.client.xml({
  cache: new evejsapi.cache.redis(),
});

XmlClient.fetch('marketstat', [['typeid', 34]])
  .then((data) => {
    console.log('data');

  })
  .catch(console.error);
```
```javascript
...
XmlClient.fetch('marketstat', [['typeid', 34], ['typeid', 35]])
  .then((data) => {
    console.log('data');

  })
  .catch(console.error);
```
```javascript
...
XmlClient.fetch('quicklook', [['typeid', 34], ['typeid', 35]])
  .then((data) => {
    console.log('data');

  })
  .catch(console.error);
```
```javascript
...
XmlClient.fetch('quicklook', [['typeid', 34], ['typeid', 35], ['usesystem', 30000142]])
  .then((data) => {
    console.log('data');

  })
  .catch(console.error);
```
```javascript
...
XmlClient.fetch('quicklook:onpath:from:Jita:to:Amarr:fortype:34')
  .then((data) => {
    console.log('data');

  })
  .catch(console.error);
```

# English Version
**Coming Soon**

'use strict';

const url = require('url');
const unitjs = require('unit.js');
const fs = require('fs');

const assert = unitjs.assert;
const fsPromise = unitjs.promisifyAll(fs);

const EveCentralClient = require('../lib/evecentral');

function parseClient(fileName, done) {
  const client = new EveCentralClient();
  let xmlData = null;

  fsPromise.readFileAsync(__dirname + '/' + fileName + '.xml', 'utf-8')
    .then(client.parse)
    .then((data) => {
      xmlData = data;
      return fsPromise.readFileAsync(__dirname + '/' + fileName + '.json', 'utf-8');
    })
    .then((data) => {
      assert.deepEqual(xmlData, JSON.parse(data));
    })
    .catch(assert.ifError)
    .finally(done)
    .done();
};


describe('evejsapi.EveCentralClient', () => {
  it('#getPathName() supports <namespace>:<resource> path format', () => {
    const client = new EveCentralClient();

    assert.equal(client.getPathName('marketstat'), '/api/marketstat');
    assert.equal(client.getPathName('quicklook'), '/api/quicklook');
    assert.equal(client.getPathName('quicklook:onpath'), '/api/quicklook/onpath');
    assert.equal(client.getPathName('quicklook:onpath:from:Jita:to:Amarr:fortype:34'), '/api/quicklook/onpath/from/Jita/to/Amarr/fortype/34');
  });

  it('#getPathName() prepends base path', () => {
    const client = new EveCentralClient();

    // with trailing slashes
    client.setUrl('http://api.eve-central.com/api/');
    assert.equal(client.getPathName('marketstat'), '/api/marketstat');
    assert.equal(client.getPathName('quicklook:onpath'), '/api/quicklook/onpath');

    // without trailing slashes
    client.setUrl('http://api.eve-central.com/api');
    assert.equal(client.getPathName('marketstat'), '/api/marketstat');
    assert.equal(client.getPathName('quicklook:onpath'), '/api/quicklook/onpath');
  });

  it('#getRequestUrl() returns URL object', () => {
    const client = new EveCentralClient();

    assert.equal(url.format(client.getRequestUrl('marketstat', [['typeid', 34]])),
      'http://api.eve-central.com/api/marketstat?typeid=34');
    assert.equal(url.format(client.getRequestUrl('marketstat', [['typeid', 34], ['typeid', 35]])),
      'http://api.eve-central.com/api/marketstat?typeid=34&typeid=35');
    assert.equal(url.format(client.getRequestUrl('marketstat')),
      'http://api.eve-central.com/api/marketstat');
  });

  it('#getCacheKey() returns URL string', () => {
    const client = new EveCentralClient();

    // console.error(client.getCacheKey(client.getRequestUrl('marketstat', [['typeid', 34], ['typeid', 35]])));

    // console.error(client.getCacheKey(client.getRequestUrl('marketstat', [['typeid', 34], ['typeid', 35]])));

    assert.equal(client.getCacheKey(client.getRequestUrl('marketstat')),
      'http://api.eve-central.com/api/marketstat');
    assert.equal(client.getCacheKey(client.getRequestUrl('marketstat', [['typeid', 34], ['typeid', 35]])),
      'http://api.eve-central.com/api/marketstat?typeid=34&typeid=35');
  });

  it('#getCacheKey() sorts query string parameters alphabetically', () => {
    const client = new EveCentralClient();
    assert.equal(client.getCacheKey(client.getRequestUrl('foo:bar', [['c', 'c'], ['b', 'b'], ['a', 'a']])),
      'http://api.eve-central.com/api/foo/bar?a=a&b=b&c=c');
    assert.equal(client.getCacheKey(client.getRequestUrl('foo:bar', [['b', 'b'], ['c', 'c'], ['a', 'a']])),
      'http://api.eve-central.com/api/foo/bar?a=a&b=b&c=c');
  });

  it('#parse() can parse simple API response', (done) => {
    parseClient('data/evecentral/simple', done);
  });
});

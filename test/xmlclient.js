'use strict';

const url = require('url');
const unitjs = require('unit.js');
const fs = require('fs');
const http = require('http');

const assert = unitjs.assert;
const fsPromise = unitjs.promisifyAll(fs);

const XmlClient = require('../lib/xmlclient');

const parseClient = function (fileName, done) {
  const client = new XmlClient();
  let xmlData = null;

  fsPromise.readFileAsync(__dirname + '/' + fileName + '.xml', 'utf-8')
    .then(client.parse)
    .then((data) => {
      xmlData = data;
      return fsPromise.readFileAsync(__dirname + '/' + fileName + '.json', 'utf-8');
    })
    .then((data) => {
      assert.deepEqual(xmlData, JSON.parse(data));
    })
    .catch(assert.ifError)
    .finally(done)
    .done();
};

 describe('evejsapi.XmlClient', () => {
   it('#getPathName() supports <namespace>:<resource> path format', () => {
     const client = new XmlClient();

     assert.equal(client.getPathName('eve:SkillTree'), '/eve/SkillTree.xml.aspx');
     assert.equal(client.getPathName('char:AccountBalance'), '/char/AccountBalance.xml.aspx');
     assert.equal(client.getPathName('OhNoez'), '/OhNoez.xml.aspx');
   });

   it('#getPathName() prepends base path', () => {
     const client = new XmlClient();

     // with trailing slashes
     client.setUrl('https://api.eveonline.com/');
     assert.equal(client.getPathName('/char/AccountBalance.xml.aspx'), '/char/AccountBalance.xml.aspx');
     assert.equal(client.getPathName('char:AccountBalance'), '/char/AccountBalance.xml.aspx');

     client.setUrl('https://api.eveonline.com/char/');
     assert.equal(client.getPathName('/AccountBalance.xml.aspx'), '/char/AccountBalance.xml.aspx');
     assert.equal(client.getPathName('AccountBalance'), '/char/AccountBalance.xml.aspx');

     // without trailing slashes
     client.setUrl('https://api.eveonline.com');
     assert.equal(client.getPathName('/char/AccountBalance.xml.aspx'), '/char/AccountBalance.xml.aspx');
     assert.equal(client.getPathName('char:AccountBalance'), '/char/AccountBalance.xml.aspx');

     client.setUrl('https://api.eveonline.com/char');
     assert.equal(client.getPathName('/AccountBalance.xml.aspx'), '/char/AccountBalance.xml.aspx');
     assert.equal(client.getPathName('AccountBalance'), '/char/AccountBalance.xml.aspx');
   });

   it('#getRequestUrl() returns URL object', () => {
     const client = new XmlClient();
     assert.equal(url.format(client.getRequestUrl('char:AccountBalance', {characterID: 1234})),
       'https://api.eveonline.com/char/AccountBalance.xml.aspx?characterID=1234');
     assert.equal(url.format(client.getRequestUrl('eve:SkillTree')),
       'https://api.eveonline.com/eve/SkillTree.xml.aspx');
     assert.equal(url.format(client.getRequestUrl('foo:bar', {c: 'c', a: 'a', b: 'b'})),
       'https://api.eveonline.com/foo/bar.xml.aspx?c=c&a=a&b=b');
   });

   it('#getRequestUrl() merges default params', () => {
     const client = new XmlClient();
     client.setParams({keyID: 'foo', vCode: 'bar'});
     assert.equal(url.format(client.getRequestUrl('char:AccountBalance', {characterID: '1234'})),
       'https://api.eveonline.com/char/AccountBalance.xml.aspx?characterID=1234&keyID=foo&vCode=bar');
   });

   it('#getCacheKey() returns URL string', () => {
     const client = new XmlClient();
     assert.equal(client.getCacheKey(client.getRequestUrl('server:ServerStatus')),
       'https://api.eveonline.com/server/ServerStatus.xml.aspx');
     assert.equal(client.getCacheKey(client.getRequestUrl('char:AccountBalance', {characterID: '12345'})),
       'https://api.eveonline.com/char/AccountBalance.xml.aspx?characterID=12345');
   });

   it('#getCacheKey() sorts query string parameters alphabetically', () => {
     const client = new XmlClient();
     assert.equal(client.getCacheKey(client.getRequestUrl('foo:bar', {c: 'c', a: 'a', b: 'b'})),
       'https://api.eveonline.com/foo/bar.xml.aspx?a=a&b=b&c=c');
     assert.equal(client.getCacheKey(client.getRequestUrl('foo:bar', {b: 'b', c: 'c', a: 'a'})),
       'https://api.eveonline.com/foo/bar.xml.aspx?a=a&b=b&c=c');
   });

   it('#parse() can parse simple API response', (done) => {
     parseClient('data/xml/simple', done);
   });

   it('#parse() can parse rowsets', (done) => {
     parseClient('data/xml/rowset', done);
   });

   it('#parse() can parse nested rowsets', (done) => {
     parseClient('data/xml/alliance-list', done);
   });

   it('#parse() can parse mutli-keyed rowsets', (done) => {
     parseClient('data/xml/multi-key', done);
   });

   it('#parse() can parse cdata', (done) => {
     parseClient('data/xml/cdata', done);
   });

   it('#parse() can parse error response', (done) => {
     const client = new XmlClient();

     fsPromise.readFileAsync(__dirname + '/data/xml/error.xml', 'utf-8')
       .then(client.parse)
       .catch((err) => {
         assert.ok(err instanceof Error);
         assert.equal(err.message, 'Must provide userID or keyID parameter for authentication.');
         assert.equal(err.code, 106);
       })
       .finally(done)
       .done();
   });

   it('#fetch() can request and parse API response', (done) => {
     const server = http.createServer();
     const client = new XmlClient({
       url: 'http://127.0.0.1:3001'
     });

     fsPromise.readFileAsync(__dirname + '/data/xml/server-status.xml', 'utf-8')
       .then((xml) => {
         return server.on('request', (request, response) => {
           if (request.url === '/server/ServerStatus.xml.aspx') {
             response.write(xml);
             response.end();
           }
         });
       })
       .then(() => {
         server.listen(3001);
       })
       .then(() => {
         return fsPromise.readFileAsync(__dirname + '/data/xml/server-status.json', 'utf-8');
       })
       .then((json) => {
         client.fetch('server:ServerStatus')
           .then((result) => {
             assert.deepEqual(result, JSON.parse(json));
             server.close();
             done();
           });
       })
       .catch(assert.ifError);
   });
 });

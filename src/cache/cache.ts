/// <reference path="../../typings/tsd.d.ts" />
'use strict';

import {ICache} from '../typings/interfaces';

import crypto = require('crypto');

class Cache implements ICache {
  constructor() {}

  /**
   * Get current Time as UNIX Timestamp
   *
   * @return {Number} Current Timestamp
   */
  public getCurrentTime(): number {
    return Math.floor((new Date()).getTime() / 1000);
  }

  public getHashedKey(key: string): string {
    return crypto.createHash('sha1').update(key).digest('hex');
  }
}

export = Cache;

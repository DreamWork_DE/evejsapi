/// <reference path="../../typings/tsd.d.ts" />
'use strict';

import {RedisClient} from 'redis';

const redis = require('redis');

import Cache = require('./cache');

class RedisCache extends Cache {

  private host: string;
  private port: number;
  private socket: string;
  private prefix: string;
  private client: RedisClient;

  constructor(options?: any) {
    super();

    options = options || {};

    if (options.socket) {
      this.setSocket(options.socket);
    } else {
      this.setHost(options.host || 'localhost');
      this.setPort(options.port || 6379);
    }

    this.setPrefix(options.prefix || 'evejsapi.cache');
  }

  /**
   * Store value in cache.
   *
   * @param {String}   key      Cache key
   * @param {String}   value    Cache Value
   * @param {Number}   duration Number of seconds this cache entry will live
   *
   * @return {Promise<boolean>}
   */
  public write(key: string, value: string, duration: number): Promise<boolean> {
    return new Promise((resolve: (val: boolean) => void, reject: (val: string) => void) => {

      const cacheKey: string = [this.getPrefix(), this.getHashedKey(key)].join('.');
      const client: RedisClient = this.getClient();

      client.set(cacheKey, value, (err: string) => {
        if (err) {
          return reject(err);
        }

        client.expire(cacheKey, duration);
        resolve(true);
      });
    });
  }

  /**
   * Read Value By key
   *
   * @param {String} key Cache key
   * @returns {Promise<string>}
   */
  public read(key: string): Promise<string> {
    return new Promise((resolve: (val: string) => void, reject: (val: string) => void) => {
      const cacheKey: string = [this.getPrefix(), this.getHashedKey(key)].join('.');
      const client: RedisClient = this.getClient();

      client.get(cacheKey, (err: string, result: string) => {
        if (err) {
          return reject(err);
        }

        resolve(result);
      });
    });
  }

  public disconnect(): void {
    if (this.client) {
      this.client.quit();
      this.client = null;
    }
  }

  /**
   * Set Redis Server Hostname
   *
   * @param {String} host
   */
  private setHost(host: string): void {
    this.host = host;
  }

  /**
   * Get Redis Server Hostname
   *
   * @returns {String}
   */
  private getHost(): string {
    return this.host;
  }

  /**
   * Set Redis Server Hostport
   *
   * @param {Number} port
   */
  private setPort(port: number): void {
    this.port = port;
  }

  /**
   * Get Redis Server Hostport
   *
   * @returns {Number}
   */
  private getPort(): number {
    return this.port;
  }

  /**
   * Set Redis Server Socket Path
   *
   * @param {String} socket
   */
  private setSocket(socket: string): void {
    this.socket = socket;
  }

  /**
   * Get Redis Server Socket Path
   *
   * @returns {String}
   */
  private getSocket(): string {
    return this.socket;
  }

  /**
   * Set Redis Cache Prefix
   *
   * @param {String} prefix
   */
  private setPrefix(prefix: string): void {
    this.prefix = prefix;
  }

  /**
   * Get Redis Cache Prefix
   *
   * @returns {String}
   */
  private getPrefix(): string {
    return this.prefix;
  }

  /**
   * Set Redis Client Instance
   *
   * @param {RedisClient} client
   */
  private setClient(client: RedisClient): void {
    this.client = client;
  }

  /**
   * Get Redis Client Instance
   *
   * @returns {RedisClient}
   */
  private getClient(): RedisClient {

    if (!this.client) {

      let clientOptions: any = {
        detect_buffers: true,
      };

      if (this.getSocket()) {
        clientOptions.path = this.getSocket();
      } else {
        clientOptions.host = this.getHost();
        clientOptions.port = this.getPort();
      }

      this.setClient(redis.createClient(clientOptions));
    }

    return this.client;
  }
}

export = RedisCache;

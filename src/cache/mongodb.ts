/// <reference path="../../typings/tsd.d.ts" />
'use strict';

import Cache = require('./cache');
import mongoose = require('mongoose');
import {Mongoose} from 'mongoose';
import {IMongoDataModel} from '../typings/interfaces';


class MongoDBCache extends Cache {

  private model: mongoose.Model<IMongoDataModel> = null;

  private host: string;
  private port: number;
  private collection: string;
  private prefix: string;

  private client: Mongoose = null;

  constructor(options?: any) {
    super();

    options = options || {};

    this.setHost(options.host || 'localhost');
    this.setPort(options.port || 27017);
    this.setCollection(options.collection || 'evejsapi');
    this.setPrefix(options.prefix || '');
  }

  protected getModel(): mongoose.Model<IMongoDataModel> {
    if (this.model === null) {
      const client: Mongoose = this.getClient();
      const DataSchema: mongoose.Schema = new mongoose.Schema({
        data: {
          required: true,
          type: String,
        },
        expireAt: {
          required: true,
          type: Date,
        },
        hash: {
          index: {
            sparse: true,
            unique: true,
          },
          required: true,
          type: String,
        },
      }, {
        collection: 'XMLData',
      });

      this.model = client.model<IMongoDataModel>('XMLData', DataSchema);
    }

    return this.model;
  }

  /**
   * Store value in cache.
   *
   * @param {String}   key      Cache key
   * @param {String}   value    Cache Value
   * @param {Number}   duration Number of seconds this cache entry will live
   *
   * @return {Promise<boolean>}
   */
  public write(key: string, value: string, duration: number): Promise<boolean> {
    return new Promise((resolve: (val: boolean) => void) => {
      const Model: mongoose.Model<IMongoDataModel> = this.getModel();
      Model.create({
        data: value,
        expireAt: new Date(Date.now() + (duration * 1000)),
        hash: this.getHashedKey(key),
      });

      this.disconnect();
      resolve(true);
    });
  }

  /**
   * Read Value By Key
   *
   * @param {String} key Cache Key
   * @returns {Promise<string>}
   */
  public read(key: string): Promise<string> {
    return new Promise((resolve: (val: string) => void) => {
      const hash = this.getHashedKey(key);
      const Model = this.getModel();

      Model
        .findOne({hash: hash})
        .exec()
        .then((found: IMongoDataModel) => {
          if (found === null) {
            this.disconnect();
            resolve(null);
          } else if (this.getCurrentTime() > Math.floor(new Date(found.expireAt).getTime() / 1000)) {
            Model.remove({ hash: found.hash }, (err) => {
              if (err) {
                console.error(err);
              }

              this.disconnect();
              resolve(null);
            });
          } else {
            this.disconnect();
            resolve(found.data);
          }
        });
    });
  }

  private setHost(host: string): void {
    this.host = host;
  }

  private getHost(): string {
    return this.host;
  }

  private setPort(port: number): void {
    this.port = port;
  }

  private getPort(): number {
    return this.port;
  }

  private setCollection(collection: string): void {
    this.collection = collection;
  }

  private getCollection(): string {
    return this.collection;
  }

  private setPrefix(prefix: string): void {
    this.prefix = prefix;
  }

  private getPrefix(): string {
    return this.prefix;
  }

  private getClient(): Mongoose {
    if (this.client === null) {
      const connectData = 'mongodb://' + this.getHost() + ':' + this.getPort() + '/' + this.getPrefix() + this.getCollection();
      this.client = mongoose.connect(connectData, (err) => {
        if (err) {
          throw err;
        }
      });
    }

    return this.client;
  }

  private disconnect() {
    this.client.disconnect();
    this.client = null;
  }
}

export = MongoDBCache;

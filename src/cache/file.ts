/// <reference path="../../typings/tsd.d.ts" />
'use strict';

import path = require('path');

const del = require('del');
const fs = require('fs-extra');

import Cache = require('./cache');

class FileCache extends Cache {

  private path: string;
  private prefix: string;

  constructor(options?: any) {
    super();

    options = options || {};
    this.setPath(options.path || '/tmp/evejsapi-cache');
    this.setPrefix(options.prefix || '');
  }

  /**
   * Store value in cache.
   *
   * @param {String}   key      Cache key
   * @param {String}   value    Cache Value
   * @param {Number}   duration Number of seconds this cache entry will live
   *
   * @return {Promise<boolean>}
   */
  public write(key: string, value: string, duration: number): Promise<boolean> {
    return new Promise((resolve: (val: boolean) => void, reject: (val: string) => void) => {
      const filePath: string = this.getFilePath(key);
      const meta: string = JSON.stringify({
        expireTime: Math.floor((new Date()).getTime() / 1000) + duration,
      });
      const data: Buffer = new Buffer([meta, value].join('\n'));

      fs.outputFile(filePath, data, (err: string) => {
        if (err) {
          return reject(err);
        }

        resolve(true);
      });
    });
  }

  /**
   * Read Value By Key
   *
   * @param {String} key Cache Key
   * @returns {Promise<string>}
   */
  public read(key: string): Promise<string> {
    return new Promise((resolve: (val: string) => void) => {
      const filePath: string = this.getFilePath(key);

      fs.readFile(filePath, 'utf8', (err: string, fileValue: string) => {
        if (err) {
          return resolve(null);
        }

        const data: string[] = fileValue.toString().split('\n', 2);
        const meta: any = JSON.parse(data[0]);

        if (this.getCurrentTime() >= meta.expireTime) {
          del(filePath, {force: true})
            .then(() => {
              return resolve(null);
            });
        } else {
          resolve(data[1]);
        }
      });
    });
  }

  private setPath(path: string): void {
    this.path = path;
  }

  private getPath(): string {
    return this.path;
  }

  private setPrefix(prefix: string): void {
    this.prefix = prefix;
  }

  private getPrefix(): string {
    return this.prefix;
  }

  private getFilePath(key: string): string {
    const hash: string = super.getHashedKey(key);
    const file: string = this.getPrefix() + hash;

    return path.join(this.getPath(), hash.substr(0, 2), hash.substr(2, 2), file);
  }
}

export = FileCache;

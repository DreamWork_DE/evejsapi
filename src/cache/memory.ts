/// <reference path="../../typings/tsd.d.ts" />
'use strict';

import {IMemoryCache} from '../typings/interfaces';

import Cache = require('./cache');

class MemoryCache extends Cache {

  private _cache: IMemoryCache[] = [];

  constructor() {
    super();
  }

  /**
   * Store value in cache.
   *
   * @param {String}   key      Cache key
   * @param {String}   value    Cache Value
   * @param {Number}   duration Number of seconds this cache entry will live
   *
   * @return {Promise<boolean>}
   */
  public write(key: string, value: string, duration: number): Promise<boolean> {
    return new Promise((resolve: (val: boolean) => void) => {
      this._cache[key] = {
        expireTime: this.getCurrentTime() + duration,
        value: value,
      };

      resolve(true);
    });
  }

  /**
   * Read Value By Key
   *
   * @param {String} key Cache Key
   * @returns {Promise<string>}
   */
  public read(key: string): Promise<string> {
    return new Promise((resolve: (val: string) => void) => {
      let value = null;

      if (this._cache[key] && this.getCurrentTime() < this._cache[key].expireTime) {
        value = this._cache[key];
      }

      resolve(value);
    });
  }
}

export = MemoryCache;

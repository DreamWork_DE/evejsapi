'use strict';

import {ICache} from './typings/interfaces';
import MemoryCache = require('./cache/memory');

import NodeUrl = require('url');
import querystring = require('querystring');
import restify = require('restify');
import lodash = require('lodash');

const packageJSON = require('./../package.json');
const assert: any = require('unit.js').assert;

abstract class AbstractClient {

  protected _url: NodeUrl.Url;
  protected _opts: any;
  protected _params: Object;
  protected _cache: ICache;

  protected _pathSuffix: string = '';

  constructor(options: any) {

    options = options || {};
    this._url = null;
    this._opts = options;
    this._params = {};

    this.setParams(options.params || {});
    this.setCache(options.cache || new MemoryCache());
  }

  protected static _hasParams(params: Object): boolean {
    for (let param in params) {
      if (params.hasOwnProperty(param)) {
        return true;
      }
    }

    return false;
  }

  /**
   * Set default Parameter
   *
   * @param {String}  param Parameter Name
   * @param {String}  value Parameter value
   */
  public setParam(param: string, value: string): void {
    this._params[param] = value;
  }

  /**
   * Set default Parameters
   *
   * @param {Object} params
   */
  public setParams(params: Object): void {
    for (let param in params) {
      if (params.hasOwnProperty(param)) {
        this.setParam(param, params[param]);
      }
    }
  }

  /**
   * @param {String} param
   * @returns {String|null}
   */
  public getParam(param: string): string {
    if (this._params.hasOwnProperty(param)) {
      return this._params[param];
    }

    return null;
  }

  /**
   * Get default parameters.
   *
   * @return {Object} Parameters object
   */
  public getParams(): Object {
    return this._params;
  }

  /**
   * Clear default parameters.
   */
  public clearParams(): void {
    for (let param in this._params) {
      if (this._params.hasOwnProperty(param)) {
        delete this._params[param];
      }
    }
  }

  /**
   * @param {ICache} cache
   *
   * @return void
   */
  public setCache(cache: ICache): void {
    this._cache = cache;
  }

  /**
   * @return {ICache} Cache Object
   */
  public getCache(): ICache {
    return this._cache;
  }

  /**
   *  Set Server Url
   *
   *  @param sUrl {String} Server Url
   */
  public setUrl(sUrl: string): void {
    this._url = NodeUrl.parse(sUrl);
  }

  /**
   * Get server URL Object.
   *
   * @return {NodeUrl.Url}         URL string or object
   */
  public getUrlObject(): NodeUrl.Url {
    return this._url;
  }

  /**
   * Get server URL.
   *
   * @return {String} URL string or object
   */
  public getUrl(): string {
    return NodeUrl.format(this._url);
  }

  public getPathName(path: string): string {
    let basePath = this.getUrlObject().pathname.replace(/^\/*|\/*$/g, '');

    if (path[0] !== '/') {
      path = path.replace(/:/g, '/') + this._pathSuffix;
    }

    if (basePath) {
      basePath = '/' + basePath;
    }

    return basePath + '/' + path.replace(/^\/*|\/*$/g, '');
  }

  /**
   * Get request URL with specified path and params as a URL object.
   *
   * @param  {String} path   Request path
   * @param  {Object} params Query string parameters
   * @return {Object}        URL object
   */
  public getRequestUrl(path: string, params: Object): Object {
    params = params || {};

    let baseUrl: NodeUrl.Url = this.getUrlObject();
    let requestUrl: any = {};
    const defaultParams: Object = this.getParams();

    for (let param in defaultParams) {
      if (defaultParams.hasOwnProperty(param)) {
        params[param] = defaultParams[param];
      }
    }

    for (let key in baseUrl) {
      if (baseUrl.hasOwnProperty(key)) {
        requestUrl[key] = baseUrl[key];
      }
    }

    requestUrl.pathname = this.getPathName(path);
    requestUrl.path = requestUrl.pathname;

    if (AbstractClient._hasParams(params)) {
      requestUrl.search = '?' + this.prepareParams(params);
      requestUrl.path += requestUrl.search;
    }

    return requestUrl;
  }

  public fetch(path: string, params: any): Promise<any> {
    return new Promise((resolve: (val: any) => void, reject: (val: restify.RestError) => void) => {

      const self = this;

      let options: any = this.getRequestUrl(path, params);

      const cacheKey: string = this.getCacheKey(options);
      const cache: ICache = this.getCache();

      return cache.read(cacheKey)
        .then((val: string) => {

          if (val !== null) {
            return resolve(JSON.parse(val));
          }

          const client: restify.HttpClient = restify.createClient({
            url: options.href,
            userAgent: packageJSON.name + 'v' + packageJSON.version,
          });

          client.get(options.path, (err: restify.RestError, req) => {

            if (err) {
              return reject(err);
            }

            req.on('result', (errResult: restify.RestError, res: any) => {

              if (errResult) {
                return reject(errResult);
              }

              res.body = '';
              res.setEncoding('utf8');
              res.on('data', function (chunk) {
                res.body += chunk;
              });

              res.on('end', function () {

                self.parse(res.body)
                  .then((result: any) => {

                    const currentTime: number = Date.parse(result.currentTime);
                    const cachedUntil: number = Date.parse(result.cachedUntil);
                    let duration: number = Math.floor((cachedUntil - currentTime) / 1000);

                    if (isNaN(duration)) {
                      duration = 300;
                    }

                    cache.write(cacheKey, JSON.stringify(result), duration)
                      .then(() => {
                        resolve(result);
                      })
                      .catch(reject);
                  })
                  .catch(reject);
              });
            });
          });
        });
    });
  }

  /**
   * Takes a URL object and returns a string that will be used as the cache key
   * for the resource located at the URL.
   *
   * Currently this just returns a URL string with query string parameters sorted
   * alphabetically.
   *
   * @param  {Object} urlObject URL object
   * @return {String}           Cache key
   */
  public getCacheKey(urlObject: NodeUrl.Url): string {
    let keys: String[] = [];
    let newUrlObject: any = {};
    let newQuery: any = {};
    let oldQuery: any = {};

    for (let key in urlObject) {
      if (urlObject.hasOwnProperty(key)) {
        newUrlObject[key] = urlObject[key];
      }
    }

    if (urlObject.search) {
      oldQuery = querystring.parse(urlObject.search.substr(1));

      for (let key in oldQuery) {
        if (lodash.has(oldQuery, key)) {
          keys.push(key);
        }
      }

      // Reconstruct query with alphabetical key ordering
      keys.sort().forEach((key: string) => {
        newQuery[key] = oldQuery[key];
      });

      // Insertion order should be guaranteed.
      newUrlObject.search = '?' + querystring.stringify(newQuery);
      newUrlObject.path = newUrlObject.pathname + newUrlObject.search;
    }

    return NodeUrl.format(newUrlObject);
  }

  protected abstract prepareParams(params: any): string;

  protected abstract parse(xml: string): Promise<Object>;
}

export = AbstractClient;

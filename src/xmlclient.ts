/// <reference path="../typings/tsd.d.ts" />
'use strict';

import {SAXStream} from 'sax';
import AbstractClient = require('./abstract-client');

import querystring = require('querystring');
import sax = require('sax');
import lodash = require('lodash');

class XmlClient extends AbstractClient {


  public constructor(options: any) {
    super(options);

    this.setUrl(this._opts.url || 'https://api.eveonline.com');
    this._pathSuffix = '.xml.aspx';
  }

  protected prepareParams(params: any): string {
    return querystring.stringify(params);
  }

  /**
   * Parses an EVE API response from either an XML string or a readable stream.
   * A callback will be invoked and passed either an error or result object.
   *
   * @param  {String|Stream} xml API response
   *
   * @return {Promise<Object>}
   */
  protected parse(xml: string): Promise<Object> {
    return new Promise((resolve: (val: Object) => void, reject: (val: Error) => void) => {

      const parser: SAXStream = sax.createStream(true, {trim: true});

      let result: any = {};
      let current: any = result;
      let parents: any[] = [];
      let currentTag: any = null;
      let keys: any = null;

      parser.on('error', parserError);
      parser.on('end', parserEnd);
      parser.on('opentag', parserOpenTag);
      parser.on('closetag', parserCloseTag);
      parser.on('text', parserText);
      parser.on('cdata', parserCData);

      function parserError(err: Error): void {
        reject(err);
      }

      function parserEnd(): void {
        let err: any = null;
        let res: any;

        if (lodash.has(result, 'eveapi.error')) {
          err = new Error(result.eveapi.error);
          err.code = result.eveapi.errorCode;

          if (lodash.has(result, 'eveapi.currentTime')) {
            err.currentTime = result.eveapi.currentTime;
          }

          if (lodash.has(result, 'eveapi.cachedUntil')) {
            err.cachedUntil = result.eveapi.cachedUntil;
          }
        } else if (!lodash.has(result, 'eveapi.result')) {
          err = new Error('Invalid API response structure.');
        } else {
          if (lodash.has(result, 'eveapi.currentTime')) {
            result.eveapi.result.currentTime = result.eveapi.currentTime;
          }

          if (lodash.has(result, 'eveapi.cachedUntil')) {
            result.eveapi.result.cachedUntil = result.eveapi.cachedUntil;
          }

          res = result.eveapi.result;
        }

        if (err) {
          reject(err);
        } else {
          resolve(res);
        }
      }

      function parserOpenTag(tag: any): void {
        currentTag = tag;
        tag.alias = tag.name;
        tag.result = current;
        parents.push(tag);

        if (tag.name === 'key') {
          for (let attr in tag.attributes) {
            if (tag.attributes.hasOwnProperty(attr)) {
              current[attr] = tag.attributes[attr];
            }
          }
        } else if (tag.name === 'row') {
          let key = keys.map((key: string) => {
            return tag.attributes[key];
          }).join(':');

          current[key] = {};
          current = current[key];

          for (let attr in tag.attributes) {
            if (tag.attributes.hasOwnProperty(attr)) {
              current[attr] = tag.attributes[attr];
            }
          }
        } else {
          if (tag.name === 'rowset') {
            keys = tag.attributes.key.split(',');
            tag.alias = tag.attributes.name;
          } else if (tag.name === 'error') {
            current.errorCode = tag.attributes.code ? tag.attributes.code : null;
          }

          current[tag.alias] = {};
          current = current[tag.alias];
        }
      }

      function parserCloseTag(): void {
        current = parents.pop().result;
        const parentTag = parents[parents.length - 1];

        if (parentTag && parentTag.name === 'rowset') {
          keys = parentTag.attributes.key.split(',');
        }
      }

      function parserText(text: string): void {
        parents[parents.length - 1].result[currentTag.name] = text;
      }

      function parserCData(text: string): void {
        if (current.cdata === undefined) {
          current.cdata = text;
        } else {
          current.cdata += text;
        }
      }

      parser.write(xml);
      parser.end();

      resolve({});
    });
  }
}

export = XmlClient;

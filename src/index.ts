'use strict';

const evejsapi = {
  cache: {
    file: require('./cache/file'),
    memory: require('./cache/memory'),
    mongo: require('./cache/mongodb'),
    redis: require('./cache/redis'),
  },
  client: {
    evecentral: require('./evecentral'),
    xml: require('./xmlclient'),
  },
};

export = evejsapi;

'use strict';
import mongoose = require('mongoose');

export interface ICache {
  getCurrentTime(): number;
  getHashedKey(key: string): string;
  write?(key: string, value: string, duration: number): Promise<boolean>;
  read?(key: string): Promise<string>;
}

export interface IMemoryCache {
  value: string;
  expireTime: number;
}

export interface IMongoData extends mongoose.Document {
  data: string;
  hash: string;
  expireAt: string;
}

export interface IMongoDataModel extends IMongoData {}

/// <reference path="../typings/tsd.d.ts" />
'use strict';

import {Stream} from 'stream';

const parseString = require('xml2js').parseString;

import AbstractClient = require('./abstract-client');

class EveCentralClient extends AbstractClient {
  public constructor(options: any) {
    super(options);

    this.setUrl(this._opts.url || 'http://api.eve-central.com/api');
  }

  protected prepareParams(params: any): string {
    let _queryString = params.map(value => {
      return value[0] + '=' + encodeURIComponent(value[1]);
    });

    return _queryString.join('&');
  }

  protected parse(xml: string): Promise<Object> {
    return new Promise((resolve: (val: Object) => void, reject: (val: Error) => void) => {
      parseString(xml, {
        explicitArray: false,
        mergeAttrs: true,
      }, (err, result) => {
        if (err) {
          return reject(err);
        }

        resolve(result.evec_api);
      });
    });
  }
}

export = EveCentralClient;

'use strict';

const gulp = require('gulp');
const gsync = require('gulp-sync')(gulp);

gulp.task('clean:temp', require('./gulp_plugins/clean-temp'));
gulp.task('clean:lib', require('./gulp_plugins/clean-lib'));
gulp.task('clean:map', require('./gulp_plugins/clean-map'));
gulp.task('copy:json', require('./gulp_plugins/copy-json'));
gulp.task('copy:js', require('./gulp_plugins/copy-js'));
gulp.task('mocha:build', require('./gulp_plugins/mocha'));
gulp.task('typescriptbabel', require('./gulp_plugins/typescript-babel'));

gulp.task('clean', ['clean:temp', 'clean:lib']);
gulp.task('copy', ['copy:json', 'copy:js']);

gulp.task('watch', function () {
  var watcher = gulp.watch(['src/**/*.ts', 'src/**/*.json', 'test/**/*.js'], ['watcher']);
  watcher.on('change', function (event) {
    console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
  });
  gulp.start('watcher');
});

gulp.task('build', gsync.sync(['clean:temp', [['clean:map', 'clean:lib', 'typescriptbabel', 'copy', 'mocha:build']]], 'build'));
gulp.task('watcher', gsync.sync(['clean:temp', [['clean:map', 'typescriptbabel', 'clean:lib', 'copy', 'mocha:build']]], 'watch'));

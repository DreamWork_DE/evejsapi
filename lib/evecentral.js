/// <reference path="../typings/tsd.d.ts" />
'use strict';

const parseString = require('xml2js').parseString;
const AbstractClient = require('./abstract-client');
class EveCentralClient extends AbstractClient {
    constructor(options) {
        super(options);
        this.setUrl(this._opts.url || 'http://api.eve-central.com/api');
    }
    prepareParams(params) {
        let _queryString = params.map(value => {
            return value[0] + '=' + encodeURIComponent(value[1]);
        });
        return _queryString.join('&');
    }
    parse(xml) {
        return new Promise((resolve, reject) => {
            parseString(xml, {
                explicitArray: false,
                mergeAttrs: true
            }, (err, result) => {
                if (err) {
                    return reject(err);
                }
                resolve(result.evec_api);
            });
        });
    }
}
module.exports = EveCentralClient;
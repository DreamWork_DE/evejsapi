/// <reference path="../../typings/tsd.d.ts" />
'use strict';

const path = require('path');
const del = require('del');
const fs = require('fs-extra');
const Cache = require('./cache');
class FileCache extends Cache {
    constructor(options) {
        super();
        options = options || {};
        this.setPath(options.path || '/tmp/evejsapi-cache');
        this.setPrefix(options.prefix || '');
    }
    /**
     * Store value in cache.
     *
     * @param {String}   key      Cache key
     * @param {String}   value    Cache Value
     * @param {Number}   duration Number of seconds this cache entry will live
     *
     * @return {Promise<boolean>}
     */
    write(key, value, duration) {
        return new Promise((resolve, reject) => {
            const filePath = this.getFilePath(key);
            const meta = JSON.stringify({
                expireTime: Math.floor(new Date().getTime() / 1000) + duration
            });
            const data = new Buffer([meta, value].join('\n'));
            fs.outputFile(filePath, data, err => {
                if (err) {
                    return reject(err);
                }
                resolve(true);
            });
        });
    }
    /**
     * Read Value By Key
     *
     * @param {String} key Cache Key
     * @returns {Promise<string>}
     */
    read(key) {
        return new Promise(resolve => {
            const filePath = this.getFilePath(key);
            fs.readFile(filePath, 'utf8', (err, fileValue) => {
                if (err) {
                    return resolve(null);
                }
                const data = fileValue.toString().split('\n', 2);
                const meta = JSON.parse(data[0]);
                if (this.getCurrentTime() >= meta.expireTime) {
                    del(filePath, { force: true }).then(() => {
                        return resolve(null);
                    });
                } else {
                    resolve(data[1]);
                }
            });
        });
    }
    setPath(path) {
        this.path = path;
    }
    getPath() {
        return this.path;
    }
    setPrefix(prefix) {
        this.prefix = prefix;
    }
    getPrefix() {
        return this.prefix;
    }
    getFilePath(key) {
        const hash = super.getHashedKey(key);
        const file = this.getPrefix() + hash;
        return path.join(this.getPath(), hash.substr(0, 2), hash.substr(2, 2), file);
    }
}
module.exports = FileCache;
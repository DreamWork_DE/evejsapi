/// <reference path="../../typings/tsd.d.ts" />
'use strict';

const Cache = require('./cache');
class MemoryCache extends Cache {
    constructor() {
        super();
        this._cache = [];
    }
    /**
     * Store value in cache.
     *
     * @param {String}   key      Cache key
     * @param {String}   value    Cache Value
     * @param {Number}   duration Number of seconds this cache entry will live
     *
     * @return {Promise<boolean>}
     */
    write(key, value, duration) {
        return new Promise(resolve => {
            this._cache[key] = {
                expireTime: this.getCurrentTime() + duration,
                value: value
            };
            resolve(true);
        });
    }
    /**
     * Read Value By Key
     *
     * @param {String} key Cache Key
     * @returns {Promise<string>}
     */
    read(key) {
        return new Promise(resolve => {
            let value = null;
            if (this._cache[key] && this.getCurrentTime() < this._cache[key].expireTime) {
                value = this._cache[key];
            }
            resolve(value);
        });
    }
}
module.exports = MemoryCache;
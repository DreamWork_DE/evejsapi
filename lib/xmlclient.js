/// <reference path="../typings/tsd.d.ts" />
'use strict';

const AbstractClient = require('./abstract-client');
const querystring = require('querystring');
const sax = require('sax');
const lodash = require('lodash');
class XmlClient extends AbstractClient {
    constructor(options) {
        super(options);
        this.setUrl(this._opts.url || 'https://api.eveonline.com');
        this._pathSuffix = '.xml.aspx';
    }
    prepareParams(params) {
        return querystring.stringify(params);
    }
    /**
     * Parses an EVE API response from either an XML string or a readable stream.
     * A callback will be invoked and passed either an error or result object.
     *
     * @param  {String|Stream} xml API response
     *
     * @return {Promise<Object>}
     */
    parse(xml) {
        return new Promise((resolve, reject) => {
            const parser = sax.createStream(true, { trim: true });
            let result = {};
            let current = result;
            let parents = [];
            let currentTag = null;
            let keys = null;
            parser.on('error', parserError);
            parser.on('end', parserEnd);
            parser.on('opentag', parserOpenTag);
            parser.on('closetag', parserCloseTag);
            parser.on('text', parserText);
            parser.on('cdata', parserCData);
            function parserError(err) {
                reject(err);
            }
            function parserEnd() {
                let err = null;
                let res;
                if (lodash.has(result, 'eveapi.error')) {
                    err = new Error(result.eveapi.error);
                    err.code = result.eveapi.errorCode;
                    if (lodash.has(result, 'eveapi.currentTime')) {
                        err.currentTime = result.eveapi.currentTime;
                    }
                    if (lodash.has(result, 'eveapi.cachedUntil')) {
                        err.cachedUntil = result.eveapi.cachedUntil;
                    }
                } else if (!lodash.has(result, 'eveapi.result')) {
                    err = new Error('Invalid API response structure.');
                } else {
                    if (lodash.has(result, 'eveapi.currentTime')) {
                        result.eveapi.result.currentTime = result.eveapi.currentTime;
                    }
                    if (lodash.has(result, 'eveapi.cachedUntil')) {
                        result.eveapi.result.cachedUntil = result.eveapi.cachedUntil;
                    }
                    res = result.eveapi.result;
                }
                if (err) {
                    reject(err);
                } else {
                    resolve(res);
                }
            }
            function parserOpenTag(tag) {
                currentTag = tag;
                tag.alias = tag.name;
                tag.result = current;
                parents.push(tag);
                if (tag.name === 'key') {
                    for (let attr in tag.attributes) {
                        if (tag.attributes.hasOwnProperty(attr)) {
                            current[attr] = tag.attributes[attr];
                        }
                    }
                } else if (tag.name === 'row') {
                    let key = keys.map(key => {
                        return tag.attributes[key];
                    }).join(':');
                    current[key] = {};
                    current = current[key];
                    for (let attr in tag.attributes) {
                        if (tag.attributes.hasOwnProperty(attr)) {
                            current[attr] = tag.attributes[attr];
                        }
                    }
                } else {
                    if (tag.name === 'rowset') {
                        keys = tag.attributes.key.split(',');
                        tag.alias = tag.attributes.name;
                    } else if (tag.name === 'error') {
                        current.errorCode = tag.attributes.code ? tag.attributes.code : null;
                    }
                    current[tag.alias] = {};
                    current = current[tag.alias];
                }
            }
            function parserCloseTag() {
                current = parents.pop().result;
                const parentTag = parents[parents.length - 1];
                if (parentTag && parentTag.name === 'rowset') {
                    keys = parentTag.attributes.key.split(',');
                }
            }
            function parserText(text) {
                parents[parents.length - 1].result[currentTag.name] = text;
            }
            function parserCData(text) {
                if (current.cdata === undefined) {
                    current.cdata = text;
                } else {
                    current.cdata += text;
                }
            }
            parser.write(xml);
            parser.end();
            resolve({});
        });
    }
}
module.exports = XmlClient;
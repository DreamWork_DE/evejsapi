'use strict';

const MemoryCache = require('./cache/memory');
const NodeUrl = require('url');
const querystring = require('querystring');
const restify = require('restify');
const lodash = require('lodash');
const packageJSON = require('./../package.json');
const assert = require('unit.js').assert;
class AbstractClient {
    constructor(options) {
        this._pathSuffix = '';
        options = options || {};
        this._url = null;
        this._opts = options;
        this._params = {};
        this.setParams(options.params || {});
        this.setCache(options.cache || new MemoryCache());
    }
    static _hasParams(params) {
        for (let param in params) {
            if (params.hasOwnProperty(param)) {
                return true;
            }
        }
        return false;
    }
    /**
     * Set default Parameter
     *
     * @param {String}  param Parameter Name
     * @param {String}  value Parameter value
     */
    setParam(param, value) {
        this._params[param] = value;
    }
    /**
     * Set default Parameters
     *
     * @param {Object} params
     */
    setParams(params) {
        for (let param in params) {
            if (params.hasOwnProperty(param)) {
                this.setParam(param, params[param]);
            }
        }
    }
    /**
     * @param {String} param
     * @returns {String|null}
     */
    getParam(param) {
        if (this._params.hasOwnProperty(param)) {
            return this._params[param];
        }
        return null;
    }
    /**
     * Get default parameters.
     *
     * @return {Object} Parameters object
     */
    getParams() {
        return this._params;
    }
    /**
     * Clear default parameters.
     */
    clearParams() {
        for (let param in this._params) {
            if (this._params.hasOwnProperty(param)) {
                delete this._params[param];
            }
        }
    }
    /**
     * @param {ICache} cache
     *
     * @return void
     */
    setCache(cache) {
        this._cache = cache;
    }
    /**
     * @return {ICache} Cache Object
     */
    getCache() {
        return this._cache;
    }
    /**
     *  Set Server Url
     *
     *  @param sUrl {String} Server Url
     */
    setUrl(sUrl) {
        this._url = NodeUrl.parse(sUrl);
    }
    /**
     * Get server URL Object.
     *
     * @return {NodeUrl.Url}         URL string or object
     */
    getUrlObject() {
        return this._url;
    }
    /**
     * Get server URL.
     *
     * @return {String} URL string or object
     */
    getUrl() {
        return NodeUrl.format(this._url);
    }
    getPathName(path) {
        let basePath = this.getUrlObject().pathname.replace(/^\/*|\/*$/g, '');
        if (path[0] !== '/') {
            path = path.replace(/:/g, '/') + this._pathSuffix;
        }
        if (basePath) {
            basePath = '/' + basePath;
        }
        return basePath + '/' + path.replace(/^\/*|\/*$/g, '');
    }
    /**
     * Get request URL with specified path and params as a URL object.
     *
     * @param  {String} path   Request path
     * @param  {Object} params Query string parameters
     * @return {Object}        URL object
     */
    getRequestUrl(path, params) {
        params = params || {};
        let baseUrl = this.getUrlObject();
        let requestUrl = {};
        const defaultParams = this.getParams();
        for (let param in defaultParams) {
            if (defaultParams.hasOwnProperty(param)) {
                params[param] = defaultParams[param];
            }
        }
        for (let key in baseUrl) {
            if (baseUrl.hasOwnProperty(key)) {
                requestUrl[key] = baseUrl[key];
            }
        }
        requestUrl.pathname = this.getPathName(path);
        requestUrl.path = requestUrl.pathname;
        if (AbstractClient._hasParams(params)) {
            requestUrl.search = '?' + this.prepareParams(params);
            requestUrl.path += requestUrl.search;
        }
        return requestUrl;
    }
    fetch(path, params) {
        return new Promise((resolve, reject) => {
            const self = this;
            let options = this.getRequestUrl(path, params);
            const cacheKey = this.getCacheKey(options);
            const cache = this.getCache();
            return cache.read(cacheKey).then(val => {
                if (val !== null) {
                    return resolve(JSON.parse(val));
                }
                const client = restify.createClient({
                    url: options.href,
                    userAgent: packageJSON.name + 'v' + packageJSON.version
                });
                client.get(options.path, (err, req) => {
                    if (err) {
                        return reject(err);
                    }
                    req.on('result', (errResult, res) => {
                        if (errResult) {
                            return reject(errResult);
                        }
                        res.body = '';
                        res.setEncoding('utf8');
                        res.on('data', function (chunk) {
                            res.body += chunk;
                        });
                        res.on('end', function () {
                            self.parse(res.body).then(result => {
                                const currentTime = Date.parse(result.currentTime);
                                const cachedUntil = Date.parse(result.cachedUntil);
                                let duration = Math.floor((cachedUntil - currentTime) / 1000);
                                if (isNaN(duration)) {
                                    duration = 300;
                                }
                                cache.write(cacheKey, JSON.stringify(result), duration).then(() => {
                                    resolve(result);
                                }).catch(reject);
                            }).catch(reject);
                        });
                    });
                });
            });
        });
    }
    /**
     * Takes a URL object and returns a string that will be used as the cache key
     * for the resource located at the URL.
     *
     * Currently this just returns a URL string with query string parameters sorted
     * alphabetically.
     *
     * @param  {Object} urlObject URL object
     * @return {String}           Cache key
     */
    getCacheKey(urlObject) {
        let keys = [];
        let newUrlObject = {};
        let newQuery = {};
        let oldQuery = {};
        for (let key in urlObject) {
            if (urlObject.hasOwnProperty(key)) {
                newUrlObject[key] = urlObject[key];
            }
        }
        if (urlObject.search) {
            oldQuery = querystring.parse(urlObject.search.substr(1));
            for (let key in oldQuery) {
                if (lodash.has(oldQuery, key)) {
                    keys.push(key);
                }
            }
            // Reconstruct query with alphabetical key ordering
            keys.sort().forEach(key => {
                newQuery[key] = oldQuery[key];
            });
            // Insertion order should be guaranteed.
            newUrlObject.search = '?' + querystring.stringify(newQuery);
            newUrlObject.path = newUrlObject.pathname + newUrlObject.search;
        }
        return NodeUrl.format(newUrlObject);
    }
}
module.exports = AbstractClient;
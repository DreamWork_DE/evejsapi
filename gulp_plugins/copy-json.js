module.exports = function() {
  'use strict';

  return this.src([
    'src/**/*.json'
  ])
    .pipe(this.dest('lib'));
};

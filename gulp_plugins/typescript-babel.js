'use strict';

const typescript = require('gulp-tsc');
const babel = require('gulp-babel');

module.exports = function () {

  return this.src(['src/**/*.ts', 'typings/**/*.ts'])
    .pipe(typescript({
      //removeComments: true,
      module: 'commonjs',
      target: 'ES6',
      noEmitOnError: true,
      tscPath: 'node_modules/typescript/bin/tsc',
      tmpDir: 'gulp_tmp',
      outDir: '.temp',
      noResolve: true
    }))
    .pipe(babel())
    .pipe(this.dest('.temp'));
};

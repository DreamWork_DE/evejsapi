'use strict';

var gmocha = require('gulp-mocha');

module.exports = function() {
  return this.src(['test/**/*.js'])
    .pipe(gmocha({
      clearRequireCache: true
    }));
};

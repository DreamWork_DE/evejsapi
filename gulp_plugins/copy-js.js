module.exports = function() {
  'use strict';

  return this.src([
    '.temp/**/*.js'
  ])
    .pipe(this.dest('lib'));
};

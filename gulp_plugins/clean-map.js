var del = require('del');

module.exports = function(cb) {
  'use strict';

  del(['.temp']).then(function () {
    cb();
  });
};
